## Fiche synthétique

### Auteurs
À l'origine du projet : Raphaël Bastide et Sarah Garcin. Pour le workshop : Étienne Ozeray et Romain Marula. Ils sont tous graphistes et développeurs, et en partie artistes (numériques). Mais également des participants...

### Esthétique
D'une part l'initiative globale de PrePostPrint : esthétique entre minimalisme et *brutalisme*.

Les projets présentés : même esprit, avec certaines choses esthétiquement plus accessibles que d'autres.

### Fabrication
L'idée est que les outils proposés peuvent être facilement pris en main, que chacun puisse s'accaparer les projets : les utiliser, les partager, les modifier. Il y a vraiment l'esprit open source derrière la démarche de PrePostPrint.

### Fonctionnel
Fabriquer des livres, ou plus globalement des publications, en se passant des outils traditionnels comme InDesign ou même Scribus. Il s'agit autant de produire que d'expérimenter.

### Usage
Il faut être en mesure d'accepter les erreurs et les difficultés produites par les outils, qui sont en perpétuelle finalisation.

### Communication
Le site web de PrePostPrint : [http://prepostprint.org/](http://prepostprint.org/)

### Commercialisation
Derrière PrePostPrint il y a également une recherche économique dans les outils – mais pas dans le temps ou les recherches nécessaires au fonctionnement de ces outils.

### Ouvertures critiques
Ambition d'influencer sur les développements des navigateurs.

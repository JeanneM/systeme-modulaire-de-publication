# Playlist
Liste des morceaux de musique écoutés pendant la réalisation de ce mémoire.

[Lebanon Hanover - Gallowdance](https://www.youtube.com/watch?v=WPw7nlluRdc)

[Shannon Wright - With Closed Eyes](https://www.youtube.com/watch?v=hfCYG0Jkq1Y)

[Human Tetris - Things I Don't Need](https://www.youtube.com/watch?v=ALk3o7m5Jt8)

[Battles - Futura](https://www.youtube.com/watch?v=9zAJ0R9zHFY)

[girlpool - Soup](https://girlpoool.bandcamp.com/track/soup)

[Swans - Bring the Sun](https://www.youtube.com/watch?v=vlONStHIBUA)

[Franz Schubert - La mort et la jeune fille](https://www.youtube.com/watch?v=qXhxi4z0bLs)

[Cigarettes After Sex - Dreaming of You](https://www.youtube.com/watch?v=ZuIDh4XIzxU)

[Shellac - Cooper](https://www.youtube.com/watch?v=t_et94mhU9s)

[Andy Shauf - Wendell Walker (Live at The Drake Hotel)](https://www.youtube.com/watch?v=3dThrMKG4r4)

[Mogwai - Olds Poisons](https://www.youtube.com/watch?v=DK6yw2xQ4y4)

[Mix - Modular ambient](https://www.youtube.com/watch?v=gOqCXuUtsbg&start_radio=1&list=RDgOqCXuUtsbg)

[r beny - Spring in Blue](https://www.youtube.com/watch?v=0we7tkr-jh4)

[Public Service Broadcasting - The Pit](https://www.youtube.com/watch?v=Z7J7AZVq_cg&t=4m42s)

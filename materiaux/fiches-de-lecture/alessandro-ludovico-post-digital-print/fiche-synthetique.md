# Fiche synthétique : Ludovico, Alessandro, Post-Digital Print

## Notice bibliographique de l'ouvrage étudié
Alessandro Ludovico  
*Post-Digital Print : la mutation de l'édition depuis 1894*  
Paris, B42, 2016

## Parties lues
Livre complet.

## Introduction
*Situer brièvement le texte dans les premières lignes (par rapport à la doctrine entière, par rapport à l'œuvre dont il est issu, par rapport à la philosophie en général), puis dans les lignes suivantes vous pourrez alors exposer le thème débouchant sur la thèse, puis sur la problématique du texte.*

## 1. Analyse des formes grammaticales ou générales

### 1.1. Présentation générale du texte (paragraphes, etc.)
Structure classique : chapitres, parties et parfois des sous-parties. Un argument principal est développé pour chaque partie, mais il peut y avoir plusieurs analyses de cas ou plusieurs exemples par partie. Les chapitres sont plutôt thématiques.

### 1.2. Termes ou expressions de liaison
Rien de particulier.

### 1.3. Formules, expressions, concepts soulignés par l'auteur (en italique, gras, etc.)
Les concepts importants sont signalés entre guillemets.

### 1.4. Ponctuation significative
Rien de particulier.

### 1.5. Structure première
Le livre est constitué de 6 chapitres, explorant les évolutions de l'imprimé et du numérique, ainsi que ses expérimentations :

- chapitre 1 : évolution de l'imprimé
- chapitre 2 : techniques d'impression dans l'édition alternative
- chapitre 3 : mutation de l'impression avec le cas de la presse
- chapitre 4 : nouvelles formes de document numérique
- chapitre 5 : l'archive comme combinaison du papier et du numérique
- chapitre 6 : le réseau comme condition d'évolution du document (livre ou magazine/revue)


## 2. Étude conceptuelle

### 2.1. Repérage des termes essentiels + 2.2. Définition des termes et concepts (dans le cadre du texte)
Il n'y a pas de concept particulier, puisque ce livre est plus un exposé qu'une thèse.

#### Objets imprimés
Façon de rassembler les différentes publications que sont le livre, la revue, le magazine, voir des formes hybrides comme le fanzine.

#### Hybridation
Combinaison dynamique en cela que les propriétés de deux éléments *combinés* se retrouvent dans le résultat hybride. Cela n'est pas forcément un objet, cela peut être une démarche globale.

#### Édition alternative
C'est l'objet de ce livre, et donc l'édition alternative consiste, pour Alessandro Ludovico, en une diffusion d'idées au sein d'"individus de même sensibilité", et ce mouvement a commencé avant Gutenberg, au moment de la gravure sur bois. La révolution de l'imprimerie ne fera que faciliter la diffusion de ces publications alternatives, majoritairement politiques jusqu'au vingtième siècle.

#### Réseau
Il s'agit de l'élément essentiel permettant à l'édition de se modifier. La définition qu'en fait Alessandro Ludovico tout au long du livre peut s'apparenter à celle d'internet : un réseau de réseaux, des nœuds liés de façon distribués.

#### Unité atomique
Expression empruntée à Marissa Mayer (alors chez Google), qui permet de définir l'article, mais qui était jusqu'ici le journal. C'est une expression de marketing qu'Alessandro Ludovico déplace du côté du contenu.

#### Geste éditorial
Pour distinguer l'activité de publication.

#### Customisation ou personnalisation
Le fait de pouvoir composer un objet imprimé ou numérique.

#### Reformatage
Il s'agit de la définition des possibilités de l'EPUB : extraire le contenu de la mise en forme.

#### Archivage
La définition de ce terme par Alessandro Ludovico est assez stricte, puisqu'il y inclut des notions d'ouverture, tant en terme de participation que d'accès, ou de pérennité.

#### Distribution
"L'édition n'existe pas sans la distribution."

#### Impression postnumérique
L'"impression postnumérique" serait donc la "combinaison" de plusieurs éléments, depuis le modèle de la souscription via internet jusqu'à "l'hybridation de l'imprimé et du numérique" en passant par l'utilisation d'un ordinateur pour publier le contenu (même sous forme imprimée).

### 2.3. Établir les rapports entre les différentes parties


### 2.4. Structure dynamique (les parties, le cheminement du raisonnement et de l'argumentation)

## 3. Thème et thèse
### 3.1. Thème
"La mutation de l'édition depuis 1894" : l'évolution des modes de publication en lien avec le numérique.

### 3.2. Thèse
Comment dépasser la dualité papier et numérique, ou transformer la rencontre du papier avec le numérique ?

## 4. Problématique
### 4.1. Questionnement
Si le numérique a bouleversé l'"objet imprimé" sur plusieurs plans, quel pourrait être le dépassement de cette simple rencontre entre papier et numérique ?

### 4.2. Problème
Comment pourrait être rendue possible une hybridation entre papier et numérique ?

### 4.3. Enjeu(x)
Exposer les différents bouleversements pour les comprendre et les appréhender, sous l'axe de l'édition alternative, véritable laboratoire des expérimentations de la publication.

## 5. Partie réflexive
### 5.1. Situation du texte dans l'histoire des idées
Alessandro Ludovico tente de prolonger la pensée de Marshall McLuhan. En terme de connivence politique ou militante, il semble très proche de mouvements italiens.

### 5.2. Intérêt philosophique du problème (et de l'éventuelle solution de l'auteur)
Approche d'expérimentations alternatives, trop rare dans ce domaine. Analyses de cas pertinentes,

### 5.3. Étude de la portée du fragment
Le livre, daté de 2012, trouve encore un intérêt en 2017. Au-delà de l'aspect historique et de certains exemples rarement présentés ailleurs, cet ouvrage est un outil pour comprendre les évolutions de l'édition, et une esquisse de ce que pourrait devenir l'édition.

### 5.4. Éventuellement, commentaires divers
Il y a parfois une faiblesse des arguments, peut-être par manque de pratique de l'auteur de certains processus ou interfaces numériques. Malgré des analyses de cas toujours pertinentes et lisibles.

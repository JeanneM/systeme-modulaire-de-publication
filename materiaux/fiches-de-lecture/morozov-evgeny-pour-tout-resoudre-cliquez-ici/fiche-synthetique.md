# Fiche synthétique : Morozov, Evgeny, Pour tout résoudre, cliquez ici

## Notice bibliographique de l'ouvrage étudié
Morozov Evgeny  
*Pour tout résoudre, cliquez ici*  
Limoges, Fyp éditions, 2014

## Parties lues
Chapitres 1, 2 et 9, ainsi que le "Post-scriptum", et partiellement le chapitre 3. L'idée est de lire la partie théorique (chapitres 1 et 2), sans les exemples, ainsi que la conclusion du livre (chapitre 9).

## Introduction
*Situer brièvement le texte dans les premières lignes (par rapport à la doctrine entière, par rapport à l'œuvre dont il est issu, par rapport à la philosophie en général), puis dans les lignes suivantes vous pourrez alors exposer le thème débouchant sur la thèse, puis sur la problématique du texte.*

## 1. Analyse des formes grammaticales ou générales

### 1.1. Présentation générale du texte (paragraphes, etc.)
Structuration classique et lisible : livres découpés en chapitres ; chapitres découpés en parties de quelques pages ; parties constituées de plusieurs paragraphes de quelques lignes.

### 1.2. Termes ou expressions de liaison
Rien de bien exotique, et cela reste relativement discret : "ainsi", "prenons exemple", "en d'autres termes", "en y regardant de plus près", etc.

### 1.3. Formules, expressions, concepts soulignés par l'auteur (en italique, gras, etc.)
Pas de signalement typographique des concepts, si ce n'est les guillemets, parfois.

### 1.4. Ponctuation significative
Rien à signaler.

### 1.5. Structure première
Le livre est construit autour de neuf chapitres : deux chapitres consacrés à la présentation de concepts critiqués, six chapitres pour illustrer cette critique, un chapitre pour dépasser tout cela.


## 2. Étude conceptuelle

### 2.1. Repérage des termes essentiels + 2.2. Définition des termes et concepts (dans le cadre du texte)

#### Silicon Valley
Il s'agit d'une zone géographique – la baie de San Francisco, en Californie, aux États-Unis – occupée par nombre d'entreprises de l'informatique, des nouvelles technologies et du numérique. Berceau de l'informatique, d'entreprises comme IBM, Intel, Adobe, Apple, Facebook ou Google. La Silicon Valley est la territorialisation des technologies de pointe.

#### Efficacité
La recherche de l'efficacité est la recherche du meilleur moyen pour réaliser une tâche ou une action, et par meilleur on peut entendre : rapidité, rentabilité, économie de moyens, visibilité, etc. La recherche d'efficacité est un perfectionnement, d'une certaine façon.

#### Innovateurs
Ce sont les acteurs de la Silicon Valley, ceux qui cherchent et trouvent des nouvelles technologies, des nouvelles applications de ces technologies. Ce sont aussi ceux qui développent un discours sur leurs propres activités.

#### Résoudre les problèmes
Cette expression est récurrente, elle est utilisée par les "innovateurs" eux-mêmes.

#### Solutionnisme
Chercher à perfectionner une activité bien définie, bien souvent à court terme. Déterminer une solution pour un problème extrait de son contexte. Evgeny Morozov porte un regard critique sur ce concept, car le "solutionnisme" peut créer plus de tord que de bien, pour deux raisons : la mauvaise compréhension du problème et la vision à court terme. Autre point important : le solutionnisme voit des problèmes là il y en a pas forcément.  
"Le rejet du solutionnisme n'est pas celui de la technologie." (p. 26)

#### Webcentrisme
Le "webcentrisme" est l'application du solutionnisme avec "l'internet" : internet serait une innovation déconnectée d'autres évolutions – il y aurait un avant et un après internet –, une chose immuable et monolithique qui ne peut être remise en question, la base et le réceptacle de toutes les innovations technologiques contemporaines, ou de tous les maux liés aux technologies. Internet possèderait une téléologie – l'effet précéderait la cause.  
Par ailleurs le "webcentrisme" ne favorise pas la diversité, mais une homogénéité propre à ce qu'internet a créé et crée pour le moment : exemple avec Kickstarter et les projets qui y sont les plus aidés qui correspondent aux champs d'intérêt de ceux qui soutiennent ces projets.  
Le webcentrisme induit de tout vouloir expliquer au prisme d'internet, sans esprit critique.

#### Époqualisme
Affirmer une révolution, quelque chose de totalement nouveau, sans l'expliquer ou le justifier, et surtout sans réellement comparer cela avec d'autres faits passés – en convoquant l'histoire –, il s'agit autant des "optimistes" que des "pessimistes".  
Le webcentrisme conduit certaines réflexions sur des bases qui sont elles-mêmes des généralisations, c'est un effet de l'"époqualisme".

#### Pensée anhistorique
Il s'agit d'une pensée qui se détache de toute référence historique.


### 2.3. Établir les rapports entre les différentes parties
La première partie rappelle le concept de "solutionnisme" et la deuxième l'applique dans le cas d'internet, avec le webcentrisme. La dernière partie (chapitre 9) tente de résumer tout le développement des chapitres consacrés aux exemples.

### 2.4. Structure dynamique (les parties, le cheminement du raisonnement et de l'argumentation)
Evgeny Morozov construit son argumentation avec de nombreux exemples, et fait appel à des penseurs de différentes époques, le plus souvent des critiques.

## 3. Thème et thèse
### 3.1. Thème
Le solutionnisme dans le cas des technologies du numérique.

### 3.2. Thèse
Le solutionnisme est une vision erronée et une application dangereuse de la technologie, alors qu'il y a d'autres théories et d'autres approches possibles.

## 4. Problématique
### 4.1. Questionnement
Est-ce que le numérique, avec internet et ses nombreuses innovations, ne seraient pas aussi formidables que les innovateurs de la Silicon Valley voudraient nous le faire croire ? Certaines expérimentations, qui utilisent des terminaux informatiques, des capteurs et le réseau internet, ne sont-elles tout simplement aberrantes ?

### 4.2. Problème
L'aberration devient danger, dans différentes temporalités : court terme (inutilité économique et d'usage), moyen terme (absence de réflexion ou de débat), long terme (disparition de références historiques, dettes). Il est nécessaire de porter un autre regard sur la technologie.

### 4.3. Enjeu(x)
Repenser notre rapport à la technologie, pour améliorer notre rapport à la technique et donc au monde.

## 5. Partie réflexive
### 5.1. Situation du texte dans l'histoire des idées
Evgeny Morozov est une voix dissidente dans ce qui semble être une irrémédiable dichotomie (optimistes vs pessimistes) ou un énorme consensus (tout est bien dans le meilleur des mondes technologiques) contemporains.  
Evgeny Morozov rejoint d'autres penseurs critiques, qu'Evgeny Morozov cite lui-même : Ivan Illitch (systèmes d'enseignement), Jane Jacobs (urbanisme), Michael Oakeshott, Hans Jonas (cybernétique), James Scott (démocratie), Friedrich Hayek.  
Evgeny Morozov s'oppose à des penseurs de la technologie ou des innovateurs contemporains, comme Jonathan Zittrain, David Weinberger, Clay Shirky, Jeff Jarvis, Tim Wu ou Elizabeth Eisenstein (historienne moins contemporaine).

### 5.2. Intérêt philosophique du problème (et de l'éventuelle solution de l'auteur)
La base de l'argumentation d'Evgeny Morozov repose sur un effort de définition, d'analyse, de réflexivité et de liens avec d'autres événements historiques. Il s'agit clairement d'une démarche philosophique, ou tout du moins critique.

Evgeny Morozov cherche à *relever le débat*, à inciter à retrouver un regard critique face à une succession de prétendues révolutions technologiques et sociales. La voix d'Evgeny Morozov est pour le moins isolée face à la surreprésentation (notamment médiatique) des écrits de ces vingt dernières années, et d'un courant technophile (ou parfois technophobe) qui semble le discours unique.

### 5.3. Étude de la portée du fragment
Les deux premières parties étudiées ici sont la base de la critique du livre d'Evgeny Morozov : expliciter le solutionnisme et ce qu'il représente aujourd'hui, et présenter le concept de "webcentrisme".

Ces deux parties permettent de construire un regard critique sur le monde numérique de la fin du vingtième siècle et du début du vingt-et-unième siècle.

### 5.4. Éventuellement, commentaires divers
Le ton d'Evgeny Morozov est parfois déroutant : parfois à la limite de l'aggressivité, peut-être moralisateur à certains moments, mais toujours pédagogique via le recours à d'autres auteurs ou expériences.

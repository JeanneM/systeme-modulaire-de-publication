---
title: "2.1. O'Reilly"
category: 2. Expérimentations
order: 2
partiep_link: /2-experimentations/2-0-introduction/
parties_link: /2-experimentations/2-2-getty-publications/
repo: _memoire/2-experimentations/-2-1-o-reilly.md
---
{% include_relative -2-1-o-reilly.md %}

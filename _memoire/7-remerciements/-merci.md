Je remercie Anthony Masure et Marcello Vitali-Rosati pour avoir accepté la co-direction de ce mémoire, et pour m'avoir apporté des retours précieux, rapides, exigeants et enthousiasmants.

Merci à Benoît Epron, cet humble travail n'est qu'un résultat parmi d'autres dans le cadre d'un Master qu'il a dirigé avec professionnalisme et humanité.

Merci à Frank Taillandier sans qui ce travail n'aurait jamais débuté, à Joël Faucilhon pour son incroyable désir de connaître et de transmettre, et à Thomas Parisot pour son temps et sa curiosité.

Merci à Julien Taquet pour sa patience, son acharnement et sa fougue.

Merci à Julie Blanc d'interroger le livre sous l'angle du design graphique.

Merci à Jennifer Bernard pour sa relecture précise.

Que soient également remerciées les précédentes promotions du Master Publication numérique qui, sans le savoir peut-être, m'ont apporté un terreau d'échanges et de réflexions inégalable.

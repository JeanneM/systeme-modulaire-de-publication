Ce mémoire fait référence à nombre d'ouvrages et d'articles scientifiques[^voir-bibliographique], ainsi qu'à d'autres textes permettant de donner un socle théorique à ce travail de recherche.
Avant d'étudier des projets et d'explorer certaines pistes, nous avons jugé nécessaire de prolonger cet exercice bibliographique par l'analyse de trois livres, et d'intégrer ces commentaires de texte _dans_ le mémoire.
Des transformations de l'édition au "progrès technique" en passant par le "solutionnisme technologique", ces trois analyses sont autant d'occasions d'interroger les mutations du livre avec un esprit critique et des approches originales.
Nous prenons le temps de présenter, résumer et disséquer trois textes, puis de proposer une courte hypothèse pour chacun d'eux : comment interroger la question des chaînes de publication avec les approches de chacun de ces auteurs ?

_Post-digital print: la mutation de l'édition depuis 1894_ {% cite ludovico_post-digital_2016 %} est un ouvrage incontournable pour qui s'intéresse aux évolutions du livre et de la presse au vingtième et au début du vingt-et-unième siècle.
Alessandro Ludovico y expose le concept d'"hybridation" à propos de la rencontre et de l'articulation entre papier et numérique, tentant de ne pas opposer ces deux mediums.
_Post-Digital Print_ est aussi un moyen d'aborder la question des formes du livre, multiples _avant_ puis _avec_ le numérique.
L'hybridation n'est-elle réalisable qu'à condition de repenser les chaînes de publication ?

_Pour tout résoudre, cliquez ici: l’aberration du solutionnisme technologique_ {% cite morozov_pour_2014 %} porte une voix dissidente par rapport aux enthousiastes – pour ne pas dire fanatiques – du numérique.
Evgeny Morozov analyse de nombreux exemples où la technologie est mal employée, utilisée comme une solution en soi sans prendre en compte les problèmes qu'elle est censée résoudre.
Comme Gilbert Simondon dont un des ouvrages majeurs est étudié dans un troisième temps de cette première partie, Evgeny Morozov défend l'idée que l'enjeu actuel n'est pas d'adhérer aveuglement ou de rejeter totalement la technologie, mais d'envisager une utilisation plus réfléchie.
Les travers présentés dans _Pour tout résoudre, cliquez ici_ peuvent être similaires dans le domaine de l'édition : l'usage systématique de logiciels monolithiques n'est-il pas problématique ?

_Du mode d'existence des objets techniques_ {% cite simondon_du_2012 %} est un texte fondateur dans le champ de la philosophie de la technique qui a influencé nombre de penseurs et d'auteurs.
Gilbert Simondon y développe une thèse critiquant une vision dichotomique de la technique, cette dernière étant soit envisagée comme uniquement utilitariste, soit sacralisée ou rejetée.
Il s'agit de repenser notre positionnement par rapport à la "machine", que la civilisation comprend mal.
À partir de la thèse de Gilbert Simondon, comment pouvons-nous reconsidérer la place de l'humain par rapport à une chaîne d'édition classique, faite de logiciels hermétiques ?

Comme précisé dans l'introduction, ces commentaires de texte ne sont pas placés en annexes, afin de traduire les principes exposés ici : ce mémoire se veut un ensemble modulaire, dont les analyses de ces trois textes constituent le fondement théorique.

[^voir-bibliographique]: Voir la bibliographie de ce mémoire.

---
title: "Version imprimée"
layout: print
---
<section class="intro">
<h1 class="chapter" id="n0">Introduction : Faire avec le numérique ?</h1>
</section>
{% include_relative _memoire/0-introduction/-introduction.md %}

<h1 class="chapter" id="n1">1. Mutations du livre</h1>

{% include_relative _memoire/1-mutations-du-livre/-1-0-introduction.md %}

<h1 class="sub-chapter" id="n1-1">1.1. Hybridation</h1>

{% include_relative _memoire/1-mutations-du-livre/-1-1-hybridation.md %}

<h1 class="sub-chapter" id="n1-2">1.2. Solutionnisme</h1>

{% include_relative _memoire/1-mutations-du-livre/-1-2-solutionnisme.md %}

<h1 class="sub-chapter" id="n1-3">1.3. Progrès technique</h1>

{% include_relative _memoire/1-mutations-du-livre/-1-3-progres-technique.md %}

<h1 class="chapter" id="n2">2. Expérimentations</h1>

{% include_relative _memoire/2-experimentations/-2-0-introduction.md %}

<h1 class="sub-chapter" id="n2-1">2.1. O'Reilly</h1>

{% include_relative _memoire/2-experimentations/-2-1-o-reilly.md %}

<h1 class="sub-chapter" id="n2-2">2.2. Getty Publications</h1>

{% include_relative _memoire/2-experimentations/-2-2-getty-publications.md %}

<h1 class="sub-chapter" id="n2-3">2.3. Distill</h1>

{% include_relative _memoire/2-experimentations/-2-3-distill.md %}

<h1 class="chapter" id="n3">3. Un système modulaire</h1>

{% include_relative _memoire/3-un-systeme-modulaire/-3-0-introduction.md %}

<h1 class="sub-chapter" id="n3-1">3.1. Les étapes d'une chaîne de publication</h1>

{% include_relative _memoire/3-un-systeme-modulaire/-3-1-les-etapes-d-une-chaine-de-publication.md %}

<h1 class="sub-chapter" id="n3-2">3.2. Les principes d'un nouveau modèle</h1>

{% include_relative _memoire/3-un-systeme-modulaire/-3-2-les-principes-d-un-nouveau-modele-de-publication.md %}

<h1 class="chapter" id="n-c">Conclusion : Pour de nouvelles dépendances</h1>

{% include_relative _memoire/5-conclusion/-conclusion.md %}

<h1 class="chapter" id="n-n">Notes</h1>
<p>Les notes sont placées à la fin de ce document, à défaut de pouvoir être intégrées en pied de page ou dans les différentes parties concernées.</p>
* footnotes will be placed here. This line is necessary
{:footnotes}

<h1 class="chapter" id="n-a">Annexes</h1>

<h1 class="biblio" id="n-a-1">Lexique</h1>

{% include_relative _memoire/6-annexes/-lexique.md %}

<h1 class="sub-annexes" id="n-a-2">Bibliographie</h1>

{% include_relative _memoire/6-annexes/-bibliographie.md %}

<h1 class="sub-annexes" id="n-a-3">Autour du mémoire</h1>

{% include_relative _memoire/6-annexes/-autour-du-memoire.md %}

<h1 class="chapter">À propos</h1>

{% include_relative _memoire/8-a-propos/-a-propos.md %}
